import sys
import traceback
import pygame
import time

COLORS = {
    'white': (255, 255, 255),
    'black': (0, 0, 0),
    'green': (0, 200, 0),
    'blue': (0, 0, 128),
    'red': (200, 0, 0),
    'purple': (102, 0, 102)
}


class Drawer:
    def __init__(self, simulation, board):
        self.simulation = simulation
        self.cell_shapes = self.create_disk_shapes(board)
        pygame.init()
        self.screen = pygame.display.set_mode((
            board.get_dims()[0] * (CELL_WIDTH + CELL_SPAN),
            board.get_dims()[1] * (CELL_WIDTH + CELL_SPAN)
        ))
        self.draw_cells()
        pygame.display.update()

    @staticmethod
    def create_disk_shapes(board):
        shapes = []
        for dims, cell in board.get_cells().items():
            shapes.append(CellShape(cell, *dims))
        return shapes

    def clear_screen(self):
        self.screen.fill(COLORS['white'])

    def show_simulation(self):
        try:
            self.simulation.simulate(self)
        except Exception as e:
            traceback.print_exc()

        while True:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    pygame.display.quit()
                    sys.exit()

    def draw_cells(self):
        self.clear_screen()
        for cell_shape in self.cell_shapes:
            pygame.draw.rect(
                self.screen,
                COLORS['black'] if cell_shape.cell.is_visited() else COLORS['purple'],
                pygame.Rect(
                    cell_shape.X,
                    cell_shape.Y,
                    CELL_WIDTH,
                    CELL_WIDTH
                )
            )
        pygame.display.update()
        pygame.event.get()
        time.sleep(1)


CELL_UNVISITED_COLOR = 'white'
CELL_VISITED_COLOR = 'blue'
CELL_WIDTH = 40
CELL_SPAN = 10


class CellShape:
    def __init__(self, cell, x, y):
        self.cell = cell
        self.X = x * (CELL_WIDTH + CELL_SPAN)
        self.Y = y * (CELL_WIDTH + CELL_SPAN)
