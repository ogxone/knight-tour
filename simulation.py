import random

from knighttour.drawer.pygame import Drawer


class Cell:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.visited = False

    def visit(self):
        self.visited = True

    def unvisit(self):
        self.visited = False

    def is_visited(self):
        return self.visited

    def get_dim(self):
        return self.x, self.y

    def get_adjacent_cells(self):
        return [
            Cell(self.x - 1, self.y + 2),
            Cell(self.x - 2, self.y + 1),
            Cell(self.x + 1, self.y + 2),
            Cell(self.x + 2, self.y + 1),

            Cell(self.x - 1, self.y - 2),
            Cell(self.x - 2, self.y - 1),
            Cell(self.x + 1, self.y - 2),
            Cell(self.x + 2, self.y - 1),
        ]

    def __eq__(self, other):
        return self.get_dim() == other.get_dim()

    def __repr__(self):
        return f'({self.x}, {self.y})'

    def is_adjacent_to(self, test_cell):
        for valid_adjacent_cell in self.get_adjacent_cells():
            if valid_adjacent_cell == test_cell:
                return True

        return False


class Board:
    def __init__(self, dims):
        self.cells = {}
        self.dims = dims

        # init cells
        for x in range(dims[0]):
            for y in range(dims[1]):
                self.cells[(x, y)] = {
                    'cell': Cell(x, y),
                    'adjacent_cells': []
                }
        # init cells interconnections
        for cell in self.cells.values():
            for adjacent_cell in cell['cell'].get_adjacent_cells():
                if adjacent_cell.get_dim() in self.cells:
                    cell['adjacent_cells'].append(self.cells[adjacent_cell.get_dim()]['cell'])

    def get_random_cell(self):
        return self.cells[(
            random.randint(0, self.dims[0] - 1),
            # 0,d
            # 0,
            random.randint(0, self.dims[1] - 1)
        )]['cell']

    def get_dims(self):
        return self.dims

    def __len__(self):
        return self.dims[0] * self.dims[1]

    def get_cells(self):
        return {cell_dim: self.cells[cell_dim]['cell'] for cell_dim in self.cells}

    def get_adjacent_cells(self, cell):
        if cell.get_dim() in self.cells:
            return self.cells[cell.get_dim()]['adjacent_cells']

        return []


class Simulation:
    def __init__(self, board):
        self.board = board
        self.last_visited_cell = None
        self.tour = []

    def simulate(self, drawer):
        self.drawer = drawer
        self.depth = 0;
        self.traverse_cells(
            self.board.get_random_cell()
        )

        for cell in board.get_cells().values():
            cell.unvisit()

        for cell in self.tour:
            cell.visit()
            self.drawer.draw_cells()

    def traverse_cells(self, cell):
        # if self.is_legal_move(cell):
        self.depth += 1
        cell.visit()
        self.tour.append(cell)
        # self.drawer.draw_cells()
        if self.depth >= len(self.board):
            return True

        # self.validate_knight_rule(cell)
        # self.last_visited_cell = cell
        # self.drawer.draw_cells()

        done = False
        for adjacent_cell in board.get_adjacent_cells(cell):
            if done:
                break
            if not adjacent_cell.is_visited():
                done = self.traverse_cells(adjacent_cell)

        if not done:
            self.tour.pop()
            cell.unvisit()
            # self.drawer.draw_cells()

        self.depth -= 1

        return done

        # if not self.is_tour_complete():

    # def is_tour_complete(self):
    #     return len(self.tour) == len(self.board)
    #
    # def is_legal_move(self, current_visited_cell):
    #     # if self.last_visited_cell is None:
    #     #     return True
    #
    #     if self.last_visited_cell is not None and not current_visited_cell.is_adjacent_to(self.last_visited_cell):
    #         return False
    #
    #     return True
    #     # raise RuntimeError(f'Cell {current_visited_cell} in not adjacent to {self.last_visited_cell}')
    #
    # def validate_knight_rule(self, current_visited_cell):
    #     if self.last_visited_cell is None:
    #         return
    #
    #     if not current_visited_cell.is_adjacent_to(self.last_visited_cell):
    #         raise RuntimeError(f'Cell {current_visited_cell} in not adjacent to {self.last_visited_cell}')


if __name__ == '__main__':
    board = Board((4, 4))
    # Simulation(Drawer()).traverse_cells(board.get_random_cell())
    Drawer(Simulation(board), board).show_simulation()
